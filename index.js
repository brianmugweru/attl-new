const express = require('express')
const app = express()

const PORT = 3000

app.get('/', (req, res, next) => res.status(200).json({ message: 'Welcome to our api' }))

app
  .listen(
    PORT, console.log(`listening on port ${PORT}`)
  )
